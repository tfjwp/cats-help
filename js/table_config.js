/*
 * Table Config
 * Datatables settings for My Requests and All requests table
 */
jQuery(document).ready(function ($) {
    $('#my-requests, #all-requests').each(function(){
        $(this).dataTable({
            "columnDefs": [
                {
                    "type": "date",
                    "targets": "date"
                }
            ],
            "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
            display:'compact',
            order: [],
            info: false
        });
    });
});