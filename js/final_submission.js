
jQuery(document).ready(function($){
    var finalizeDraft = $("#field_cr-finalize_submission-0");
    var draftStatus = $("#field_cr-status");

    if (draftStatus.val() == "Submitted"){
        finalizeDraft.prop('checked', true);
    }

    finalizeDraft.change(function() {
        if (finalizeDraft.is(':checked')) {
            draftStatus.val("Submitted");
        } else {
            draftStatus.val("Draft");
        }
    });
});