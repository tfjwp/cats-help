<?php
/**
 * WP-Starter functions and definitions
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage WP_Starter
 * @since WP-Starter 4.0
 */

 /*
  * Register datatables stylesheets
  */
wp_register_style( 'datatables_css', '//cdn.datatables.net/1.10.16/css/jquery.dataTables.css', array(), '1.10.16', 'all' );
wp_register_script( 'datatables_js', "//cdn.datatables.net/1.10.16/js/jquery.dataTables.js", array('jquery'), '1.10.16', false );

 /**
  * Enqueue our child-theme style sheets
  */
function wpstarter_child_style() 
{
    wp_dequeue_style('wpforge');
    wp_enqueue_style('parent-styles', get_template_directory_uri() . '/style.css', '', '5.5.2.5');
    wp_enqueue_style('child-styles', get_stylesheet_uri(), array( 'parent-styles' ), '4.0');
}
add_action( 'wp_enqueue_scripts', 'wpstarter_child_style');


/**
 * Enqueue our datatables assets
 */
function load_table_assets()
{
    if (is_page( array('cats-change-requests', 'cats-change-requests-my-requests' ))){
		wp_enqueue_style('datatables_css');
        wp_enqueue_script('datatables_js');
        wp_enqueue_script('table_config', get_theme_file_uri() . '/js/table_config.js',array('jquery'),'0.0.1',false);
    }
}
add_action( 'wp', 'load_table_assets');


/**
 * CATS Submission message script 
*/
function load_cr_form_submission_message_script(){
    if(is_page( 'cats-change-request-form' )){
        wp_dequeue_style('wpforge');
		wp_enqueue_script('change_submission_status', get_theme_file_uri() . '/js/final_submission.js',array('jquery'), '0.0.1',false);
    }    
}
add_action( 'wp', 'load_cr_form_submission_message_script');


function restrict_suppliers_from_requests(){
    if (is_page( array('cats-change-requests')) && is_supplier()){
            wp_redirect(get_site_url());
    }
}
add_action( 'wp', 'restrict_suppliers_from_requests');

function is_supplier(){
	$supplier_roles = array('supplier_cenveo','supplier_integra','supplier_spi','supplier_techset');
    switch_to_blog("8"); // Production Hub blog id
    $caps = get_currentuserinfo()->allcaps;
    $has_supplier_role = false;
    foreach(array_keys($caps) as &$cap){
		if (in_array($cap,$supplier_roles) && $caps[$cap]){
			$has_supplier_role = true;
            break;
		}
	}
	restore_current_blog();
	return($has_supplier_role);
}

// only "EPICS Admins", "Administrators", and "Epics Submitters" may submit change requests
add_action('frm_display_form_action', 'cats_change_request_access', 8, 3);
function cats_change_request_access($params, $fields, $form){
  remove_filter('frm_continue_to_new', '__return_false', 50);
  global $wpdb;
  $cats_request_form_key = 'cats_request';
  $cats_request_form_id = $wpdb->get_var( $wpdb->prepare(
          "SELECT id 
           FROM ". $wpdb->prefix ."frm_forms 
           WHERE form_key = %s", $cats_request_form_key
      ) 
  );
    
  if($form->id == $cats_request_form_id){
    if(!(current_user_can('epics_admin') || current_user_can('administrator')|| current_user_can('epics_submitter'))){
      echo "You don't have the required role to submit an entry to this form.";
      add_filter('frm_continue_to_new', '__return_false', 50);
    }
  }
}


/**
 * CATS EPICs Submissions
 * Users may edit their CATs EPICs submissions until they check a box saying that their draft submission 
 * is final. Then, only admins and 'CATS admins' may edit and update forms.
 * change IDs for deployment to live
 */
add_filter('frm_user_can_edit', 'maybe_prevent_user_edit_entry', 10, 2);
function maybe_prevent_user_edit_entry( $edit, $args )
{
    if ( ! $edit ) {
        // user can't edit anyway, so don't check
        return $edit;
    }
  
    global $wpdb;
    $cats_request_form_key = 'cats_request';
    $cats_request_form_id = $wpdb->get_var( $wpdb->prepare(
            "SELECT id 
             FROM ". $wpdb->prefix ."frm_forms 
             WHERE form_key = %s", $cats_request_form_key
        ) 
    );

    // CATS EPIC form
    if ( $args['form']->id != $cats_request_form_id ) {
        return $edit;
    }

    if ( is_numeric( $args['entry'] ) ) {
        $entry_id = $args['entry'];
    } else {
        $entry_id = $args['entry']->id;
    }

    $status_key = 'cr-status';
    $status_field_id = $wpdb->get_var( $wpdb->prepare(
            "SELECT id 
             FROM ". $wpdb->prefix ."frm_fields 
             WHERE field_key = %s", $status_key
        ) 
    );
    $draft_status = FrmProEntriesController::get_field_value_shortcode( 
        array( 'field_id' => $status_field_id,
        'entry' => $entry_id 
        ) 
    );

    if( !(current_user_can('epics_admin') || current_user_can('administrator')) ){
        if ( $draft_status != 'Draft' ) {
        $edit = false;
        }
    }
    return $edit;
}


/*
 * Shortcode to limit conent to Administrators and CATS admins
 * https://stackoverflow.com/questions/39593273/user-roles-shortcode
 * defaults to Administrator or EPICS Admin, but one may specificy  list of 
 * roles with a comma separated string.  
 */
function filter_content_by_role( $atts, $content = null ) 
{
    shortcode_atts(array('roles'=> array('epics_admin','administrator')), $atts);

    $roles = $atts['roles'];
    if (is_string($roles)){
        $roles = explode(',', $roles);
    }

    foreach ($roles as $role) {
       if( current_user_can($role) ){
           return $content;
       }
    }
}
add_shortcode( 'admin_content', 'filter_content_by_role' );


/*
 * Filter out closed statuses by default 
 */
add_filter('frm_where_filter', 'default_filter_closed', 10, 2);
function default_filter_closed($where, $args)
{
    global $wpdb;
    $statuses = ["draft","submitted","analysis", "blocked", "in development", "complete", "closed"];
    
    $status_key = 'cr-status';
    $my_requests_view_key = 'my-change-requests';
    $all_requests_view_key = 'all-change-requests';
    $my_requests_view_id = $wpdb->get_var( $wpdb->prepare(
            "SELECT ID 
             FROM {$wpdb->posts} 
             WHERE post_name = %s", $my_requests_view_key
        )
    );
    $all_requests_view_id = $wpdb->get_var( $wpdb->prepare(
            "SELECT ID 
             FROM {$wpdb->posts} 
             WHERE post_name = %s", $all_requests_view_key 
        )
    );
    $status_field_id = $wpdb->get_var( $wpdb->prepare(
            "SELECT id 
            FROM ". $wpdb->prefix ."frm_fields 
            WHERE field_key = %s", $status_key
        ) 
    );

    // filter closed by default, otherwise only show what's in the get param
    $filter = '';
    if ( !(in_array($args['where_val'], $statuses)) ) {
        $filter = "meta_value <> 'Closed'";
    } else {
        $filter = "meta_value = '". $args['where_val'] ."'";
    }
    
    if ( ($args['display']->ID == $my_requests_view_id ) && $args['where_opt'] == $status_field_id ) {
        $where = sprintf("( %s and fi.id = %d)",$filter,$status_field_id);
    }

    // only admins can see drafts on the all requests page
    if ( ($args['display']->ID == $all_requests_view_id) && $args['where_opt'] == $status_field_id ) {
        if ( !(current_user_can('epics_admin') || current_user_can('administrator')) ) {
            $filter = $filter . " and " . "meta_value <> 'Draft'" ;
        }
        $where = sprintf("( %s and fi.id = %d)",$filter,$status_field_id);
    } 
    return $where;  
}

add_filter( 'the_content', 'tgm_io_shortcode_empty_paragraph_fix' );
/**
 * Filters the content to remove any extra paragraph or break tags
 * caused by shortcodes.
 *
 * @since 1.0.0
 *
 * @param string $content  String of HTML content.
 * @return string $content Amended string of HTML content.
 */
function tgm_io_shortcode_empty_paragraph_fix( $content ) 
{
    $array = array(
        '<p>['    => '[',
        ']</p>'   => ']',
        ']<br />' => ']'
    );
    
    return strtr( $content, $array );
}

// // this function has been run once, but the changes were permanently made in the
// // database. Replace add_cap with remove_cap to reverse these changes.
// function add_theme_caps() {
//     $role = get_role('epics_admin');
//     $role->add_cap('list_users'); 
// 	$role->add_cap('promote_users');
// }
// add_action( 'admin_init', 'add_theme_caps');

?>

